import logging
import unittest
import itertools

import collections

import sliding


Request = collections.namedtuple("Request", ("id", "data"))


class Protocol(sliding.Protocol):

    _logger = logging.getLogger("sliding.tests.protocol")

    def __init__(self):
        self.request_seq = itertools.count()
        self.reset()

    def reset(self):
        # All packets sent through `send`
        self.request_sent = []
        # Packets awaiting response
        self.awaiting_response = collections.OrderedDict()
        # Packets responded through `recv`
        self.responded = collections.OrderedDict()

    def send(self, data, previous_id=None):
        packet_id = (
            next(self.request_seq)
            if previous_id is None
            else previous_id
        )
        self.request_sent.append(data)
        self.awaiting_response[packet_id] = data
        self._logger.info("sending Packet(id=%s, data=%s)", packet_id, data)
        return packet_id

    def should_drop(self, req_id, data):
        return False

    def recv(self, timeout):
        data, req_id = self._recv()
        if self.should_drop(req_id, data):
            self._logger.info(
                "dropping response for Request(id=%s, data=%s", req_id, data)
            raise sliding.TimeoutError()
        self._logger.info("returning Response(id=%s)", req_id)
        self.responded[req_id] = data
        return req_id

    def _recv(self):
        return self.awaiting_response.popitem(last=False)


class TestCase(unittest.TestCase):

    def setUp(self):
        super(TestCase, self).setUp()
        self.configure_logging()

    @staticmethod
    def configure_logging():
        if len(logging.root.handlers) > 0:
            return
        fmt = "%(asctime)s [%(levelname)8s] %(name)s: %(message)s"
        formatter = logging.Formatter(fmt)
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(formatter)
        logging.root.addHandler(console_handler)
        logging.root.setLevel(logging.DEBUG)
