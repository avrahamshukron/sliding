from glob import glob

from os.path import join, dirname, splitext, basename
from setuptools import setup, find_packages


def read(*path):
    return open(join(dirname(__file__), *path)).read()


version = {}
exec(
    read("src", "sliding", "version.py"),
    version
)  # Now we have `__version__` in `version`

setup(
    name='sliding',
    version=version["__version__"],
    packages=find_packages('src'),
    package_dir={"": "src"},
    py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
    url='git@gitlab.com:avrahamshukron/sliding.git',
    license='GNU GPLv3',
    author='Avraham Shukron',
    author_email='avraham.shukron@gmail.com',
    description="Protocol-agnostic, efficient sliding transmission window "
                "implementation ",
    long_description=read("README.md"),
    include_package_data=True,
)
